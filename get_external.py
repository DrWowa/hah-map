#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import urllib.request as urlr
import os


def get_tiles():
    counter = 0
    known = set()
    work = set(((0, 0),))  # {(0,0)}
    itr = (-1, 0, 1)
    for name in os.listdir(PATH):
        _, xx, yy = name[:-4].split('_')
        known.add((xx, yy))

    while work:
        point = work.pop()
        for x in itr:
            for y in itr:
                tile = (point[0]+x, point[1]+y)
                if tile not in known:
                    print('\rdownloaded: {:>6} tiles, work at {:>4} {:>4}'
                            .format(counter, tile[0], tile[1]), end='')
                    try:
                        name = 'tile_{}_{}.png'.format(tile[0], tile[1])
                        urlr.urlretrieve(URL.format(x=tile[0], y=tile[1]),
                                         os.path.join(PATH, name))
                    except:
                        continue
                    else:
                        known.add(tile)
                        work.add(tile)
                        counter += 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='steal HaH map')
    parser.add_argument('target')
    args = parser.parse_args()
    if args.target == 'apxeolog':
        PATH = 'apxeolog'
        URL = 'http://map.apxeolog.com/tiles/5/tile_{x}_{y}.png'
    if args.target == 'odditown':
        PATH = 'odditown'
        URL = 'http://www.odditown.com:8080/haven/tiles/live/9/{x}_{y}.png'
    if not os.path.exists(PATH):
        os.mkdir(PATH)
    get_tiles()
