#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from mapster import *
from raw import RAW


EXCLUDE = (
        'ff4c76027930f61300826d34ec1c11b8377773a4a0d7d64cb901941996e63b68490fe6623d040a41e9910a001cd6d51b97be5e480cebf43cfb8a3f026f237dec',
)

sqal_session = sqal_orm.create_session(bind=DB, autocommit=False)
qall = sqal_session.query(Surface).all()
hsl, hsr = set(), set(RAW)
for tl in qall:
    if tl.hs in hsl:
        hsr.add(tl.hs)
    else:
        hsl.add(tl.hs)

ff = open('raw.py', 'w')
ff.write('RAW = [\n')
for hs in hsr:
    if hs not in EXCLUDE:
        ff.write('{0}{1}{0},\n'.format("'", hs))
ff.write(']')
ff.close()
