#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from os.path import join as pJoin

from PIL import Image

from mapster import TILES, SURFACE, ZOOM_MAX


def fix_shave():
    path_tiles = pJoin(TILES, SURFACE, ZOOM_MAX)
    for xx in os.listdir(path_tiles):
        for name in os.listdir(pJoin(path_tiles, xx)):
            size = Image.open(pJoin(path_tiles, xx, name)).size
            ch_x = 100 - size[0]
            ch_y = 100 - size[1]
            if ch_x or ch_y:
                yy = name[:-4]
                os.remove(pJoin(path_tiles, xx, name))
                print(xx, yy)

if __name__ == '__main__':
    fix_shave()
