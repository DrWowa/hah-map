// https://docs.google.com/spreadsheets/d/1JgfuGfk5gUHQfTcvnxEUVybybW7H1y4nriB_F8dV_6A/edit?usp=sharing
// https://docs.google.com/spreadsheets/d/1JgfuGfk5gUHQfTcvnxEUVybybW7H1y4nriB_F8dV_6A/pubhtml
// roads styles
var highway = {color: '#00ff00',};
    highbuild = {color: '#ffff00',};
    byway = {color: '#ff00ff',};

// markers
var MCityTurtles = L.ExtraMarkers.icon({icon:'fa-university',markerColor:'green',shape:'square',prefix:'fa'});
    MCityFriends = L.ExtraMarkers.icon({icon:'fa-university',markerColor:'green-light',shape:'square',prefix:'fa'});
    MCityNeutral = L.ExtraMarkers.icon({icon:'fa-university',markerColor:'cyan',shape:'square',prefix:'fa'});
    MCampFriends = L.ExtraMarkers.icon({icon:'fa-home',markerColor:'green-light',shape:'square',prefix:'fa'});
    MCampNeutral = L.ExtraMarkers.icon({icon:'fa-home',markerColor:'cyan',shape:'square',prefix:'fa'});
    MCampAbandoned = L.ExtraMarkers.icon({icon:'fa-eject',markerColor:'black',shape:'square',prefix:'fa'});

    MToponim = L.ExtraMarkers.icon({icon:'fa-tree',markerColor:'violet',shape:'circle',prefix:'fa'});
    MCave = L.ExtraMarkers.icon({icon:'fa-arrow-circle-down',markerColor:'black',shape:'circle',prefix:'fa'});

    MResOre = L.ExtraMarkers.icon({icon:'fa-diamond',markerColor:'black',shape:'penta',prefix:'fa'});
    MResClay = L.ExtraMarkers.icon({icon:'fa-cubes',markerColor:'orange',shape:'penta',prefix:'fa'});
    MResClayRed = L.ExtraMarkers.icon({icon:'fa-cubes',markerColor:'orange-dark',shape:'penta',prefix:'fa'});
    MResEarth = L.ExtraMarkers.icon({icon:'fa-leaf',markerColor:'green-dark',shape:'penta',prefix:'fa'});
    MResWater = L.ExtraMarkers.icon({icon:'fa-tint',markerColor:'pink',shape:'penta',prefix:'fa'});
    MResStone = L.ExtraMarkers.icon({icon:'fa-gavel',markerColor:'blue-dark',shape:'penta',prefix:'fa'});
    MResSand = L.ExtraMarkers.icon({icon:'fa-hourglass',markerColor:'yellow',shape:'penta',prefix:'fa'});

    MSight = L.ExtraMarkers.icon({icon:'fa-binoculars',markerColor:'pink',shape:'star',prefix:'fa'});

// DATA
var citiesData = {'type': 'FeatureCollection', 'features':[
    //{'type':'Feature','properties':{'name':'','icon':MCityTurtles},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
]};

var toponimsData = {'type': 'FeatureCollection', 'features':[
    //{'type':'Feature','properties':{'name':'','icon':MToponim},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
]};

var roadsData = {'type': 'FeatureCollection', 'features':[
    //{'type':'Feature','properties':{'name':'Road','style':highway},'geometry':{'type':'LineString','coordinates':[[0.000,0.000],[0.000,0.000]]}},
]};

var resourcesData = {'type': 'FeatureCollection', 'features':[
    // Ores
    //{'type':'Feature','properties':{'name':'Iron Ochre','icon':MResOre},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
    // Clay
    //{'type':'Feature','properties':{'name':'Clay Y','icon':MResClay},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
    // Red clay
    //{'type':'Feature','properties':{'name':'Red Clay','icon':MResClayRed},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
    // Earth
    //{'type':'Feature','properties':{'name':'Earth','icon':MResEarth},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
    // Water
    //{'type':'Feature','properties':{'name':'Water','icon':MResWater},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
    // Stone quarry
    //{'type':'Feature','properties':{'name':'','icon':MResStone},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
]};

var sightsData = {'type': 'FeatureCollection', 'features':[
    //{'type':'Feature','properties':{'name':'','icon':MSight},'geometry':{'type':'Point','coordinates':[0.000,0.000]}},
]};

function init() {
  Tabletop.init( { key: '1JgfuGfk5gUHQfTcvnxEUVybybW7H1y4nriB_F8dV_6A',
                   callback: worker,
                   parseNumbers: true,
                } )
};

function worker(data, tabletop) {
    console.log(data);
    var ee;

    // cities
    ee = data.cities.elements
    for (var i = 0; i < ee.length; i++) {
        if (ee[i].Town_Turtles) citiesData.features.push({'type':'Feature','properties':{'name':ee[i].Town_Turtles,'icon':MCityTurtles},'geometry':{'type':'Point','coordinates':[ee[i].tt_x,ee[i].tt_y]}});
        if (ee[i].Town_friendly) citiesData.features.push({'type':'Feature','properties':{'name':ee[i].Town_friendly,'icon':MCityFriends},'geometry':{'type':'Point','coordinates':[ee[i].tf_x,ee[i].tf_y]}});
        if (ee[i].Town_neutral) citiesData.features.push({'type':'Feature','properties':{'name':ee[i].Town_neutral,'icon':MCityNeutral},'geometry':{'type':'Point','coordinates':[ee[i].tn_x,ee[i].tn_y]}});
        if (ee[i].Camp_friendly) citiesData.features.push({'type':'Feature','properties':{'name':ee[i].Camp_friendly,'icon':MCampFriends},'geometry':{'type':'Point','coordinates':[ee[i].cf_x,ee[i].cf_y]}});
        if (ee[i].Camp_neutral) citiesData.features.push({'type':'Feature','properties':{'name':ee[i].Camp_neutral,'icon':MCampNeutral},'geometry':{'type':'Point','coordinates':[ee[i].cn_x,ee[i].cn_y]}});
        if (ee[i].Abandoned) citiesData.features.push({'type':'Feature','properties':{'name':ee[i].Abandoned,'icon':MCampAbandoned},'geometry':{'type':'Point','coordinates':[ee[i].a_x,ee[i].a_y]}});
    };
    L.geoJson(citiesData, {pointToLayer: pointToLayer}).addTo(cities);

    // toponims
    ee = data.toponims.elements
    for (var i = 0; i < ee.length; i++) {
        if (ee[i].Toponim) toponimsData.features.push({'type':'Feature','properties':{'name':ee[i].Toponim,'icon':MToponim},'geometry':{'type':'Point','coordinates':[ee[i].x,ee[i].y]}});
        if (ee[i].Cave) toponimsData.features.push({'type':'Feature','properties':{'name':ee[i].Cave,'icon':MCave},'geometry':{'type':'Point','coordinates':[ee[i].cx,ee[i].cy]}});
    };
    L.geoJson(toponimsData, {pointToLayer: pointToLayer}).addTo(toponims);

    // resources
    ee = data.resources.elements
    for (var i = 0; i < ee.length; i++) {
        if (ee[i].Ores) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Ores,'icon':MResOre},'geometry':{'type':'Point','coordinates':[ee[i].o_x,ee[i].o_y]}});
        if (ee[i].Clay) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Clay,'icon':MResClay},'geometry':{'type':'Point','coordinates':[ee[i].cy_x,ee[i].cy_y]}});
        if (ee[i].Red_Clay) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Red_Clay,'icon':MResClayRed},'geometry':{'type':'Point','coordinates':[ee[i].cr_x,ee[i].cr_y]}});
        if (ee[i].Earth) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Earth,'icon':MResEarth},'geometry':{'type':'Point','coordinates':[ee[i].e_x,ee[i].e_y]}});
        if (ee[i].Water) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Water,'icon':MResWater},'geometry':{'type':'Point','coordinates':[ee[i].w_x,ee[i].w_y]}});
        if (ee[i].Stone) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Stone,'icon':MResStone},'geometry':{'type':'Point','coordinates':[ee[i].s_x,ee[i].s_y]}});
        if (ee[i].Sand) resourcesData.features.push({'type':'Feature','properties':{'name':ee[i].Sand,'icon':MResSand},'geometry':{'type':'Point','coordinates':[ee[i].sn_x,ee[i].sn_y]}});
    };
    L.geoJson(resourcesData, {pointToLayer: pointToLayer}).addTo(resources);

    // sights
    ee = data.sights.elements
    for (var i = 0; i < ee.length; i++) {
        if (ee[i].Abyssal_Chasm) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Abyssal_Chasm,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].ac_x,ee[i].ac_y]}});
        if (ee[i].Ancient_Windthrow) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Ancient_Windthrow,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].aw_x,ee[i].aw_y]}});
        if (ee[i].Clay_Pit) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Clay_Pit,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].cp_x,ee[i].cp_y]}});
        if (ee[i].Geyser) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Geyser,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].gs_x,ee[i].gs_y]}});
        if (ee[i].Heartwood) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Heartwood,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].hw_x,ee[i].hw_y]}});
        if (ee[i].Ice_Spire) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Ice_Spire,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].is_x,ee[i].is_y]}});
        if (ee[i].Rock_Crystal) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Rock_Crystal,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].rc_x,ee[i].rc_y]}});
        if (ee[i].Salt_Basin) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Salt_Basin,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].sb_x,ee[i].sb_y]}});
        if (ee[i].Sea_Shell) sightsData.features.push({'type':'Feature','properties':{'name':ee[i].Sea_Shell,'icon':MSight},'geometry':{'type':'Point','coordinates':[ee[i].ss_x,ee[i].ss_y]}});
    };
    L.geoJson(sightsData, {pointToLayer: pointToLayer}).addTo(sights);
};
init();

