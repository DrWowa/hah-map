# Description #

This repository contains set of tools to work with [H&H](http://www.havenandhearth.com/portal/) map.

## Dependencies ##

* Python 3
* ImageMagick
* Pillow
* SQLAlchemy
