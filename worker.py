#!env/bin/python
# -*- coding: utf-8 -*-
import os
import shutil
import time

from config import LISTEN_DIR
from mapster import Worker, SESS, NEW, TILES


Worker().makedirs()

while True:
    zlist = os.listdir(LISTEN_DIR)
    if not zlist:
        time.sleep(30)
        continue
    
    for zname in zlist:
        if zname.split('.')[-1] != 'zip':
            continue
        path = os.path.join(LISTEN_DIR, zname)
        shutil.unpack_archive(path, SESS)
        Worker().process_sessions()
        Worker().zoom_generate()
        for sdir, dirs, files in os.walk(NEW):
            ddir = sdir.replace(NEW, TILES, 1)
            os.makedirs(ddir, exist_ok=True)
            for ff in files:
                shutil.copy(os.path.join(sdir, ff), os.path.join(ddir, ff))

        for sdir in os.listdir(NEW):
            shutil.rmtree(os.path.join(NEW,sdir))

        os.remove(path)
