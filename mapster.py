#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import hashlib as hl
import os
import shutil
import subprocess as sbp
import sys
import tempfile
import re
from os.path import join as pJoin

from PIL import Image

import sqlalchemy as sqal
import sqlalchemy.orm as sqal_orm
from sqlalchemy.ext.declarative import declarative_base

from raw import RAW


DIR = os.path.dirname(os.path.realpath(__file__))
SESS = pJoin(DIR, 'sessions')
TILES = pJoin(DIR, 'tiles')
NEW = pJoin(DIR, 'new_tiles')
SURFACE = 'surface'
CAVE1 = 'cave1'
LAYERS = (SURFACE, CAVE1)
TILE_TMP = pJoin(tempfile.gettempdir(), 'tmp.png')
TILE_FILL = pJoin(DIR, 'db', 'fill.png')
TILE_BAD = pJoin(DIR, 'db', 'bad.png')
TILE_UNDER = pJoin(DIR, 'db', 'under.png')
DB_PATH = pJoin(DIR, 'db', 'base.db')
DB = sqal.create_engine('sqlite:///' + DB_PATH)
ZOOM_MAX = '0'
ZOOM_MIN = '-9'
PERC = 5
rg_SESS = re.compile(r'^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}\.[0-9]{2}\.[0-9]{2}$')

Base = declarative_base()


class Surface(Base):
    __tablename__ = 'tiles_surface'
    id = sqal.Column(sqal.Integer, primary_key=True)
    x = sqal.Column(sqal.Integer)
    y = sqal.Column(sqal.Integer)
    dt = sqal.Column(sqal.String(20))
    hs = sqal.Column(sqal.String(1024))
    __tableargs__ = (sqal.UniqueConstraint('x', 'y'),)


class Cave1(Base):
    __tablename__ = 'tiles_cave1'
    id = sqal.Column(sqal.Integer, primary_key=True)
    x = sqal.Column(sqal.Integer)
    y = sqal.Column(sqal.Integer)
    dt = sqal.Column(sqal.String(20))
    hs = sqal.Column(sqal.String(1024))
    __tableargs__ = (sqal.UniqueConstraint('x', 'y'),)


class Sample(object):
    def __init__(self, x_coord, y_coord, date, hash_summ, file_path):
        self.x = x_coord
        self.y = y_coord
        self.dt = date
        self.hs = hash_summ
        self.path = file_path

    def __str__(self):
        return 'sm_{}_{}'.format(self.x, self.y)


class Worker(object):
    """ Main class """
    def makedirs(self):
        pathes = (
            pJoin(NEW, SURFACE, ZOOM_MAX),
            pJoin(NEW, CAVE1, ZOOM_MAX),
            pJoin(TILES, SURFACE, ZOOM_MAX),
            pJoin(TILES, CAVE1, ZOOM_MAX),
            'db',
        )
        for path in pathes:
            os.makedirs(path, exist_ok=True)

    def base_refill(self, sess='2015-09-01 00.00.00'):
        shutil.copy2(DB_PATH, DB_PATH+'.bak')
        os.remove(DB_PATH)
        Base.metadata.create_all(DB)
        sqal_session = sqal_orm.create_session(bind=DB, autocommit=False)

        for layer in LAYERS:
            if layer == SURFACE:
                layer_cls = Surface
            elif layer == CAVE1:
                layer_cls = Cave1
            else:
                sys.exit()
            path = pJoin(TILES, layer, ZOOM_MAX)
            for xx in os.listdir(path):
                for name in os.listdir(pJoin(path, xx)):
                    sample = self._sample_new(sess, name,
                                              pJoin(path, xx, name))
                    tile = layer_cls(x=sample.x, y=sample.y, dt=sample.dt,
                                     hs=sample.hs)
                    sqal_session.add(tile)
        sqal_session.commit()
        sqal_session.close()

    def create_database(self):
        Base.metadata.create_all(DB)
        sqal_session = sqal_orm.create_session(bind=DB, autocommit=False)

        path = os.listdir(SESS)[0]
        for file_name in os.listdir(pJoin(SESS, path)):
            sample = self._sample_new(path, file_name)
            tile = Surface(x=sample.x, y=sample.y, dt=sample.dt, hs=sample.hs)
            sqal_session.add(tile)
            self._move_new(sample)
        sqal_session.commit()
        sqal_session.close()
        os.rmdir(pJoin(SESS, path))
        self.zoom_generate()

    def zoom_generate(self, layer=SURFACE):
        """Generate all zoom levels for selected layer"""
        for zoom in range(int(ZOOM_MAX), int(ZOOM_MIN), -1):
            # mkdir for next zoom
            path = pJoin(NEW, layer, str(zoom-1))
            if not os.path.exists(path):
                os.mkdir(path)

            path = pJoin(NEW, layer, str(zoom))
            x_list = [int(kk) for kk in os.listdir(path)]

            if x_list:
                self._zoom_make(layer, zoom, 0,  0, x_list)
                self._zoom_make(layer, zoom, 0, -1, x_list)
                self._zoom_make(layer, zoom, -1,  0, x_list)
                self._zoom_make(layer, zoom, -1, -1, x_list)
            else:
                break  # nothing to zoom

    def _zoom_make(self, layer, zoom, x_first, y_first, x_list):
        """Make one quarter of zoom-1 level"""
        # direction for iterations
        x_step = 1 if x_first == 0 else -1
        y_step = 1 if y_first == 0 else -1

        x_border = max(x_list) if x_step == 1 else min(x_list)
        xx = x_first
        while abs(xx) <= abs(x_border):
            if not(xx in x_list or xx+x_step in x_list):
                xx += 2 * x_step
                continue

            y_list = []
            if xx in x_list:
                path = pJoin(NEW, layer, str(zoom), str(xx))
                y_list.extend([int(kk[:-4]) for kk in os.listdir(path)])
            if xx+x_step in x_list:
                path = pJoin(NEW, layer, str(zoom), str(xx+x_step))
                y_list.extend([int(kk[:-4]) for kk in os.listdir(path)])

            x_zoomed = xx if x_step == 1 else xx-1
            path = pJoin(NEW, layer, str(zoom-1), str(x_zoomed//2))
            yy = y_first
            y_border = max(y_list) if y_step == 1 else min(y_list)
            while abs(yy) <= abs(y_border):
                if not (yy in y_list or yy+y_step in y_list):
                    yy += 2 * y_step
                    continue

                if not os.path.exists(path):
                    os.mkdir(path)

                img1 = self._path_choose(xx, yy, layer, zoom) or TILE_FILL
                img2 = self._path_choose(xx+x_step, yy,
                                         layer, zoom) or TILE_FILL
                img3 = self._path_choose(xx, yy+y_step,
                                         layer, zoom) or TILE_FILL
                img4 = self._path_choose(xx+x_step, yy+y_step,
                                         layer, zoom) or TILE_FILL

                imgL = [img1, img2, img3, img4]
                if x_step == -1:
                    imgL = imgL[-3::-1] + imgL[:-3:-1]
                if y_step == -1:
                    imgL = imgL[2:] + imgL[:2]

                y_zoomed = yy if y_step == 1 else yy-1
                path_y = pJoin(path, '{}.png'.format(y_zoomed//2))
                sbp.call(['montage', imgL[0], imgL[1], imgL[2], imgL[3],
                          '-geometry', '50x50+0+0', '-tile', '2x2', path_y])
                yy += 2 * y_step
            xx += 2 * x_step

    def _path_choose(self, xx, yy, layer=SURFACE, zoom=ZOOM_MAX):
        """Choose path for tile in TILES or NEW"""
        path = pJoin(layer, str(zoom), str(xx), '{}.png'.format(yy))
        pathT = pJoin(NEW, path)
        if os.path.exists(pathT):
            return pathT
        pathT = pJoin(TILES, path)
        if os.path.exists(pathT):
            return pathT
        return None

    def _sample_new(self, sess_name, file_name, path=None):
        """Create new Sample instance"""
        split = file_name[:-4].split('_')
        if len(split) == 3:
            _, xx, yy = split
        else:
            xx = os.path.split(os.path.split(path)[0])[1]
            yy = split[0]
        path = path or pJoin(SESS, sess_name, file_name)
        sbp.call(['convert', path, '-shave', '1x1', TILE_TMP])
        hash_summ = hl.sha512(Image.open(TILE_TMP).tobytes()).hexdigest()
        return Sample(int(xx), int(yy), sess_name, hash_summ, path)

    def _move_new(self, sample, layer=SURFACE):
        """Move new tile from session to NEW"""
        if not os.path.exists(pJoin(NEW, layer, ZOOM_MAX, str(sample.x))):
            os.mkdir(pJoin(NEW, layer, ZOOM_MAX, str(sample.x)))
        shutil.move(sample.path,
                 pJoin(NEW, layer, ZOOM_MAX,
                       str(sample.x), str(sample.y)+'.png'))

    def process_sessions(self):
        sys.stdout.write('sessions: added/under/total/(-bad), tiles: added/total\n')
        totSessions = 0
        totAdded = 0
        totTiles = 0
        totProcessed = 0
        totUnder = 0
        totBad = 0

        sess_list = os.listdir(SESS)
        path = 'currentsession.js'
        if path in sess_list:
            sess_list.pop(sess_list.index(path))
            os.remove(pJoin(SESS, path))
        sess_list.sort()

        totSessions = len(sess_list)

        for sess_name in sess_list:
            # renamed sessions
            if not rg_SESS.match(sess_name):
                shutil.rmtree(pJoin(SESS, sess_name))
                totBad += 1
                continue

            # small session
            if len(os.listdir(pJoin(SESS, sess_name))) < 16:
                shutil.rmtree(pJoin(SESS, sess_name))
                totBad += 1
                continue

            start_path = pJoin(SESS, sess_name, 'tile_0_0.png')
            # broken session
            if not os.path.exists(start_path):
                shutil.rmtree(pJoin(SESS, sess_name))
                totBad += 1
                continue

            # check bad
            test = self._compare(start_path, TILE_BAD)
            if int(test) < 5000:
                shutil.rmtree(pJoin(SESS, sess_name))
                totBad += 1
                continue

            # check under
            test = self._compare(start_path, TILE_UNDER)
            if int(test) < 5000:
                shutil.rmtree(pJoin(SESS, sess_name))
                totUnder += 1
                continue

            # generate sample list
            sample_list = {}
            x_set, y_set = set(), set()
            key = False
            for start_name in os.listdir(pJoin(SESS, sess_name)):
                sample = self._sample_new(sess_name, start_name)
                sample_list.update({sample.__str__(): sample})
                # partially bad
                test = self._compare(sample.path, TILE_BAD)
                if int(test) < 5000:
                    key = True
                    break
                # partially under
                test = self._compare(sample.path, TILE_UNDER)
                if int(test) < 5000:
                    key = True
                    break
                x_set.add(sample.x)
                y_set.add(sample.y)

            totProcessed += len(sample_list)
            if key:
                shutil.rmtree(pJoin(SESS, sess_name))
                totBad += 1
                continue

            # check disrupted
            if (max(x_set)-min(x_set) >= len(x_set) or
                    max(y_set)-min(y_set) >= len(y_set)):
                shutil.rmtree(pJoin(SESS, sess_name))
                totBad += 1
                continue

            # search shared point
            sqal_sess = sqal_orm.create_session(bind=DB, autocommit=False)
            change_x, change_y = None, None
            for sample_name in sample_list:
                sample = sample_list[sample_name]
                if sample.hs in RAW:
                    continue
                tile_list = sqal_sess.query(Surface)\
                                     .filter_by(hs=sample.hs).all()
                for tile in tile_list:
                    path = self._path_choose(tile.x, tile.y)
                    test = self._compare(path, sample.path)
                    if (int(test) < 400 and
                            self._around(tile, sample, sample_list)):
                        change_x = tile.x - sample.x
                        change_y = tile.y - sample.y
                if change_x is not None:
                    break

            if change_x is not None:
                for _, sample in sample_list.items():
                    sample.x += change_x
                    sample.y += change_y
                    tile = sqal_sess.query(Surface)\
                                    .filter_by(x=sample.x, y=sample.y).first()
                    if tile:
                        # update time
                        if sample.dt > tile.dt:
                            tile.dt = sample.dt
                            sqal_sess.add(tile)
                        else:
                            continue  # ignore old

                        if sample.hs == tile.hs:
                            continue  # ignore not modified

                        path = self._path_choose(tile.x, tile.y)
                        test = self._compare(path, sample.path)
                        if int(test) > 9000:
                            continue  # ignore bad teleport

                        tile.hs = sample.hs
                    else:
                        tile = Surface(x=sample.x, y=sample.y,
                                       dt=sample.dt, hs=sample.hs)

                    self._move_new(sample)
                    sqal_sess.add(tile)
                    totTiles += 1

                sqal_sess.commit()
                totAdded += 1

                shutil.rmtree(pJoin(SESS, sess_name))

            sys.stdout.write('\rsessions:{add:>4}/{under:>4}/{totsess:>4}/(-{bad:>4}), tiles:{tiles:>5}/{tottiles:>5}'
                             .format(add=totAdded, under=totUnder,
                                     totsess=totSessions, bad=totBad,
                                     tiles=totTiles, tottiles=totProcessed))
        sys.stdout.write('\n')

    def _compare(self, path1, path2):
        _, test = sbp.Popen(['compare', '-metric', 'AE',
                             path1, path2, TILE_TMP],
                            stderr=sbp.PIPE).communicate()
        return test

    def _around(self, tile, sample, sample_list, step=1):
        key = 0
        tot_comp = 0
        total = pow(2*step+1, 2) - pow(2*(step-1)+1, 2)
        itr = range(-step, step+1)
        for xx in itr:
            for yy in itr:
                if abs(xx) != step and abs(yy) != step:
                    continue
                name = 'sm_{}_{}'.format(sample.x+xx, sample.y+yy)
                path = self._path_choose(tile.x+xx, tile.y+yy)
                if name in sample_list and path:
                    sample_test = sample_list[name]
                    # if sample_test.hs in RAW:
                        # continue
                    tot_comp += 1
                    test = self._compare(path, sample_test.path)
                    if int(test) < PERC*100:
                        key += 1
        if key > total/2 or key == tot_comp:
            return True
        elif step < 2:
            return self._around(tile, sample, sample_list, step+1)
        else:
            return False

if __name__ == '__main__':
    # null run
    if not os.path.exists(SESS):
        os.mkdir(SESS)
        sys.stdout.write('place _START_ session in "{}" dir'.format(SESS))
        sys.exit()
    # first run
    if not os.path.exists(NEW):
        Worker().makedirs()
        Worker().create_database()
        sys.exit()
    # normal run
    Worker().makedirs()
    parser = argparse.ArgumentParser(description='manage HaH map')
    parser.add_argument('-r', action='store_true', help='refill database')
    parser.add_argument('-z', action='store_true',
                        help='generate zoom ({} only)'.format(NEW))
    parser.add_argument('-d', default='2015-09-01 00.00.00',
                        help='date-time for refill database')
    args = parser.parse_args()
    key = True
    if args.r:
        Worker().base_refill(args.d)
        key = False
    if args.z:
        Worker().zoom_generate()
        key = False
    if key:
        sbp.call(['cp', DB_PATH, DB_PATH+'.bak'])
        Worker().process_sessions()
        Worker().zoom_generate()
