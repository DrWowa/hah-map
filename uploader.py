#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import ftplib
import os
import re
import shutil

from config import FTP_SECRET, FTP_SERVER, FTP_USER


rg_SESS = re.compile(r'^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}\.[0-9]{2}\.[0-9]{2}$')


def main():
    DIR = os.path.dirname(os.path.realpath(__file__))
    MAP = os.path.join(DIR, 'map')
    TMP = os.path.join(DIR, 'tmp')
    ARC = os.path.join(DIR, 'archive')
    sess_list = os.listdir(MAP)
    CURRENT = None

    if not os.path.exists(ARC):
        os.mkdir(ARC)
    if not os.path.exists(TMP):
        os.mkdir(TMP)

    # sorting sessions
    sess_name = 'currentsession.js'
    if sess_name in sess_list:
        sess_list.pop(sess_list.index(sess_name))
        with open(os.path.join(MAP, sess_name)) as ff:
            CURRENT = ff.read().split("'")[1]

    for sess_name in sess_list:
        if not rg_SESS.match(sess_name):
            continue

        sess_len = len(os.listdir(os.path.join(MAP, sess_name)))
        # current copy, don`t move
        if sess_name == CURRENT:
            if sess_len < 16:
                continue
            shutil.copytree(os.path.join(MAP, sess_name), TMP)
        # small session
        elif sess_len < 16:
            shutil.rmtree(os.path.join(MAP, sess_name))
        else:
            shutil.move(os.path.join(MAP, sess_name), TMP)

    # creating zip
    fname = '{}'.format(datetime.datetime.now())
    path = os.path.join(ARC, fname)
    shutil.make_archive(path, 'zip', TMP)
    shutil.rmtree(TMP)

    # upload on ftp
    ftp = ftplib.FTP(FTP_SERVER, FTP_USER, FTP_SECRET)
    ftp.storbinary('STOR {}.part'.format(fname), open(path+'.zip', 'rb'))
    ftp.rename('{}.part'.format(fname), '{}.zip'.format(fname))


if __name__ == '__main__':
    main()
